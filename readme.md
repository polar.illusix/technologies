## Technologies parser

This application is based on laravel framework and it is designed for parse terms from [techopedia.com](https://www.techopedia.com/)

## Start to parse

* For parse definitions
```
php artisan parseDefinitions
```

* For parse related terms
 ```
 php artisan parseRelatedTerms
 ```

* For parse tags
```
php artisan parseTags
```

_If you want to parse related terms or tags you have to parse definitions first!_