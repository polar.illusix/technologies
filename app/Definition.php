<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Definition extends Model
{
    public $timestamps = false;
    
    public function relatedTerms()
    {
        return $this->belongsToMany('App\Definition', 'related_terms', 'definition_id', 'related_definition_id');
    }

    public function primaryTags()
    {
        return $this->belongsToMany('App\Tag')->wherePivot('primary', '=', 1);
    }
     public function allTags()
     {
         return $this->belongsToMany('App\Tag')->withPivot('primary');
     }
}
