<?php

namespace App\CustomClasses;

class Logger
{
    const color_green = "\033[0;32m";
    const color_red = "\033[0;31m";
    const end_color = "\033[0m";
    const LOG_FILE = "app/Logs/technologies.log";

    public static function setInfo($message) {
        //echo self::color_green . $message  .self::end_color . "\n";
        file_put_contents(self::LOG_FILE, $message. "\n", FILE_APPEND);
    }

    public static function setError($message) {
        //echo self::color_red . $message . self::end_color . "\n";
        file_put_contents(self::LOG_FILE, $message. "\n", FILE_APPEND);
    }
}