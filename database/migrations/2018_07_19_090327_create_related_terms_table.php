<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRelatedTermsTable extends Migration
{
    public function up()
    {
        Schema::create('related_terms', function (Blueprint $table) {
            $table->unsignedInteger('definition_id')->references('id')->on('definitions');
            $table->unsignedInteger('related_definition_id')->references('id')->on('definitions');
            $table->foreign('definition_id')->references('id')->on('definitions');
            $table->foreign('related_definition_id')->references('id')->on('definitions');

        });
    }

    public function down()
    {
        Schema::dropIfExists('related_terms');
    }
}
