<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDefinitionTagTable extends Migration
{
    public function up()
    {
        Schema::create('definition_tag', function (Blueprint $table) {
            $table->unsignedInteger('definition_id')->references('id')->on('definitions');
            $table->unsignedInteger('tag_id')->references('id')->on('tags');
            $table->boolean('primary')->default(0);;
            $table->foreign('definition_id')->references('id')->on('definitions');
            $table->foreign('tag_id')->references('id')->on('tags');

        });
    }

    public function down()
    {
        Schema::dropIfExists('definition_tag');
    }
}
