<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDefinitionsTable extends Migration
{
    public function up()
    {
        Schema::create('definitions', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->longText('meaning');
            $table->longText('explains');
            $table->string('url');
        });
    }

    public function down()
    {
        Schema::dropIfExists('definitions');
    }
}
